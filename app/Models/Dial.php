<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dial extends Model
{
    use HasFactory;

    public function campaign()
    {
    	return $this->belongsto('App\Models\Campaign', 'campaign_id');
    }

    public function user()
    {
    	return $this->belongsto('App\Models\User', 'user_id');
    }
}
