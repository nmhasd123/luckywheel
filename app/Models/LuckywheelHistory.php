<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LuckywheelHistory extends Model
{
    use HasFactory;

    public function campaign()
    {
    	return $this->belongsto('App\Models\Campaign', 'campaign_id');
    }

    public function product1()
    {
    	return $this->belongsto('App\Models\Product', 'product_id');
    }
}
