<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $pathViewController = 'product';
    private $controllerName		= 'ProductController';


    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $items = Product::all();
        return view($this->pathViewController.'.index',compact('items'));
    }

    public function getCreate()
    {
        $campaigns = Campaign::where('deleted', 0)->get();
        return view('product.create', compact('campaigns'));
    }

    public function postCreate(Request $req)
    {

                $item = new Product();
                $this->validate($req,
                [
                    'name'=>'required',
                    'code'=>'number',
                    'campaign_id'=>'required',
                ],);
                $item->name = $req->name;
                $item->quantity = $req->quantity;
                $item->unit = $req->unit;
                $item->campaign_id = $req->campaign_id;

                //lưu hình anh
                $getimage = '';
                if($req->hasFile('image'))
                {
                    //Hàm kiểm tra dữ liệu
                    $this->validate($req,
                        [
                            //Kiểm tra đúng file đuôi .jpg,.jpeg,.png.gif và dung lượng không quá 2M
                            'image' => 'mimes:jpg,jpeg,png,gif|max:2048',
                        ],
                        [
                            //Tùy chỉnh hiển thị thông báo không thõa điều kiện
                            'image.mimes' => 'Chỉ chấp nhận với đuôi .jpg .jpeg .png .gif',
                            'image.max' => 'Hình ảnh giới hạn dung lượng không quá 2M',
                        ]
                    );

                    //Lưu hình ảnh
                    $image = $req->file('image');
                    $getimage = $image->getClientOriginalName();//echo $getimage;
                    $imagename = time().rand(1,100).'.'.pathinfo($getimage, PATHINFO_EXTENSION);
                    $destinationPath = 'images/';//var_dump( $destinationPath);
                    $image->move($destinationPath, $imagename);
                    $item->image = $imagename;
                }
                $item->save();
        return redirect('products')->with('thanhcong','Tạo thành công');
    }

    public function getUpdate($product_id)
    {
        $product = Product::find($product_id);
        return view($this->pathViewController.'.update',compact('product'));
    }

    public function postUpdate($product_id ,Request $req)
    {
        $item = Product::find($product_id);
        $this->validate($req,
                [
                    'name'=>'required',
                    'code'=>'number',
                ],
                []);

                $item->name = $req->name;
                $item->quantity = $req->quantity;
                $item->unit = $req->unit;

                //lưu hình anh
                $getimage = '';
                if($req->hasFile('image'))
                {
                    //Hàm kiểm tra dữ liệu
                    $this->validate($req,
                        [
                            //Kiểm tra đúng file đuôi .jpg,.jpeg,.png.gif và dung lượng không quá 2M
                            'image' => 'mimes:jpg,jpeg,png,gif|max:2048',
                        ],
                        [
                            //Tùy chỉnh hiển thị thông báo không thõa điều kiện
                            'image.mimes' => 'Chỉ chấp nhận với đuôi .jpg .jpeg .png .gif',
                            'image.max' => 'Hình ảnh giới hạn dung lượng không quá 2M',
                        ]
                    );

                    //Lưu hình ảnh
                    $image = $req->file('image');
                    $getimage = $image->getClientOriginalName();
                    $imagename = time().rand(1,100).'.'.pathinfo($getimage, PATHINFO_EXTENSION);
                    $destinationPath = 'images/';
                    $image->move($destinationPath, $imagename);
                    $item->image = $imagename;
                }
                $item->save();
        return redirect('products')->with('thanhcong','Tạo thành công');
    }

    public function getDel($product_id )
    {
        $item = Product::find($product_id );
        $item->delete();
        return redirect('products')->with('thanhcong','Đã xóa thành công');
    }
}
