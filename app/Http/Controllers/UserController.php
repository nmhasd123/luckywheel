<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $items = User::where('active', 0)->get();
        $campaigns = Campaign::where('deleted', 0)->get();
        return view('user.index',compact('items', 'campaigns'));
    }

    public function getCreate()
    {
        return view('user.create');
    }

    public function postCreate(Request $req)
    {
        $item = new User();
        $this->validate($req,
        [
            'userName'=>'required',
            'fullName'=>'required',
            'phoneNumber'=>'required',
            'sponsor'=>'required',
            'sponsorId'=>'required',
            'sponsorAddress'=>'required',
            'sponsorFloor'=>'required',
            'placement'=>'required',
            'placementId'=>'required',
            'placementAddress'=>'required',
            'placementFloor'=>'required',
            'type'=>'required',
        ],);
        $item->userName = $req->userName;
        $item->fullName = $req->fullName;
        $item->phoneNumber = $req->phoneNumber;
        $item->email = $req->email;
        $item->avatar = $req->avatar;
        $item->sponsor = $req->sponsor;
        $item->sponsorId = $req->sponsorId;
        $item->sponsorAddress = $req->sponsorAddress;
        $item->sponsorFloor = $req->sponsorFloor;
        $item->active = $req->active;
        $item->placement = $req->placement;
        $item->placementId = $req->placementId;
        $item->placementAddress = $req->placementAddress;
        $item->placementFloor = $req->placementFloor;
        $item->type = $req->type;
        $item->save();
        return redirect('users')->with('thanhcong','Tạo thành công');
    }

    public function getUpdate($user_id)
    {
        $user = User::find($user_id);
        return view('user.update', compact('user'));
    }

    public function postUpdate($user_id, Request $req)
    {
        $item = User::find($user_id);
        $this->validate($req,
        [
            'userName'=>'required',
            'fullName'=>'required',
            'phoneNumber'=>'required',
            'sponsor'=>'required',
            'sponsorId'=>'required',
            'sponsorAddress'=>'required',
            'sponsorFloor'=>'required',
            'placement'=>'required',
            'placementId'=>'required',
            'placementAddress'=>'required',
            'placementFloor'=>'required',
            'type'=>'required',
        ],);
        $item->userName = $req->userName;
        $item->fullName = $req->fullName;
        $item->phoneNumber = $req->phoneNumber;
        $item->email = $req->email;
        $item->avatar = $req->avatar;
        $item->sponsor = $req->sponsor;
        $item->sponsorId = $req->sponsorId;
        $item->sponsorAddress = $req->sponsorAddress;
        $item->sponsorFloor = $req->sponsorFloor;
        $item->active = $req->active;
        $item->placement = $req->placement;
        $item->placementId = $req->placementId;
        $item->placementAddress = $req->placementAddress;
        $item->placementFloor = $req->placementFloor;
        $item->type = $req->type;
        $item->save();
        return redirect('users')->with('thanhcong','Lưu thành công');
    }

    public function getDel($user_id )
    {
        $item = User::find($user_id );
        $item->active = 1;
        $item->save();
        return redirect('users')->with('thanhcong','Đã xóa thành công');
    }
}
