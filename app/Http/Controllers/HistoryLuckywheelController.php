<?php

namespace App\Http\Controllers;

use App\Models\Dial;
use App\Models\LuckywheelHistory;
use App\Models\Product;
use Illuminate\Http\Request;

class HistoryLuckywheelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $items = LuckywheelHistory::all();
        return view('Luckywheel.history', compact('items'));
    }

    public function postCreate(Request $req)
    {
        $item = new LuckywheelHistory();
        // $dial = Dial::find("$req->dial_id");
        // var_dump( $this->check($req->dial_id)); exit();
        if($req->dial_id == NULL || $req->dial_id = "")
        {
            return -1;
        }
        $check = $this->check($_REQUEST['dial_id']);
        if($check == 0)
        {
            $this->validate($req,
            [
                'product_id'=>'required',
                'dial_id'=>'required',
            ],
            [
                'product_id.required'=>'Có lỗi xảy ra!',
                'dial_id.required'=>'Có lỗi xảy ra!',
            ]);
            $dial = Dial::find($_REQUEST['dial_id']);
            $item->campaign_id = $_REQUEST['campaign_id'];
            $item->user_id = $_REQUEST['user_id'];
            $item->name = $dial->user->fullName ?? "";
            $item->phone = $dial->user->phoneNumber ?? "";
            $product = Product::find($_REQUEST['product_id']);
            //trừ số lượng giải thưởng
            $product->quantity = $product->quantity - 1;
            $product->save();
            $item->product = $product->name;
            $item->product_id = $_REQUEST['product_id'];
            $item->save();
            //trừ lượt quay
            $dial->turn = $dial->turn - 1;
            $dial->Save();
            return 0;
        }
        return $check;
    }

    public function check(String $dial_id)
    {
        $dial = Dial::find($dial_id);
        if($dial->turn < 1)
        {
            //hết lượt quay
            return 1;
        }
        if(strtotime(date('Y-m-d H:i:s',strtotime($dial->campaign->start))) > strtotime(date('Y-m-d H:i:s')) )
        {
            //chưa bắt đầu
            return 2;
        }
        if(strtotime(date('Y-m-d H:i:s',strtotime($dial->campaign->end))) < strtotime(date('Y-m-d H:i:s')) )
        {
            // đã kết thúc
            return 3;
        }
        return 0;
    }
}
