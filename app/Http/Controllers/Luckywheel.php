<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Dial;
use App\Models\LuckywheelHistory;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Luckywheel extends Controller
{
    // public function __construct()
    // {
    //   $this->middleware('guest:web', ['except' => ['logout']]);
    // }

    public function getLogin($campaign_id)
    {
        $campaign = Campaign::where('id',"$campaign_id")->where('deleted', 0)->first();
        if($campaign != null && $campaign != "")
        {
            if(Auth::check())
            {
                $user = Auth::user();
                $listProduct = Product::where('campaign_id',$campaign_id)->limit(8)->get();
                $history = LuckywheelHistory::where('deleted', 0)->where('campaign_id',$campaign_id)->orderBy('created_at', 'DESC')->limit(10)->get();
                $dial = Dial::where('user_id',$user->id)->where('campaign_id', $campaign_id)->where('deleted', 0)->first();
                return view('luckywheel.index',compact('user','listProduct', 'history', 'dial', 'campaign'));
            }
            return view('login.login');
        }
        return "Sự kiện không tồn tại!";
    }

    public function getList()
    {
        $campaigns = Campaign::where('deleted', 0)->get();
        return view('campaign.list', compact('campaigns'));
    }

    // public function login($campaign_id, Request $request)
    // {
    //     $credentials = [
    //         'userName' => $request['username'],
    //         'password' => $request['password'],
    //     ];
    //     if (Auth::attempt($credentials)) {
    //         $user = Auth::user();

    //         $listProduct = Product::where('campaign_id',$campaign_id)->limit(8)->get();
    //         $history = LuckywheelHistory::where('deleted', 0)->where('campaign_id',$campaign_id)->orderBy('created_at', 'DESC')->limit(10)->get();
    //         $campaign = Campaign::find($campaign_id);

    //         $dial = Dial::where('user_id',$user->id)->where('campaign_id', $campaign_id)->where('deleted', 0)->first();
    //         return view('luckywheel.index',compact('user','listProduct', 'history', 'dial'));
    //     } else {
    //         return redirect()->back()->withInput();
    //     }
    // }

    public function login($campaign_id, Request $request)
    {
        $campaign = Campaign::find($campaign_id);
        if($campaign != null && $campaign != "")
        {
            if(Auth::check())
            {
                $user = Auth::user();
                $listProduct = Product::where('campaign_id',$campaign_id)->limit(8)->get();
                $history = LuckywheelHistory::where('deleted', 0)->where('campaign_id',$campaign_id)->orderBy('created_at', 'DESC')->limit(10)->get();
                $dial = Dial::where('user_id',$user->id)->where('campaign_id', $campaign_id)->where('deleted', 0)->first();
                return view('luckywheel.index',compact('user','listProduct', 'history', 'dial', 'campaign'));
            }
            $token = $this->getToken($request);
            if ($token != null && $token != "") {
                $userAPI = json_decode($this->getInfoUser($token))->result;

                // $user->userName;
                // $user->fullName;
                // $user->active;
                // $user->id;
                $user = User::where('id_server', $userAPI->id)->first();
                if($user == NULL || $user == "")
                {
                    $user = new User();
                    $user->id_server = $userAPI->id;
                    $user->email = $userAPI->userContact->email ?? NULL;
                    $user->phoneNumber = $userAPI->phoneNumber ?? NULL;
                    $user->fullName = $userAPI->fullName ?? "";
                    $user->userName = $userAPI->userName ?? NULL;
                    $user->sponsor = $userAPI->userAffiliate->sponsor ?? NULL;
                    $user->sponsorId = $userAPI->userAffiliate->sponsorId ?? NULL;
                    $user->sponsorAddress = $userAPI->userAffiliate->sponsorAddress ?? NULL;
                    $user->sponsorFloor = $userAPI->userAffiliate->sponsorFloor ?? NULL;
                    $user->placement = $userAPI->userAffiliate->placement ?? NULL;
                    $user->placementId = $userAPI->userAffiliate->placementId ?? NULL;
                    $user->placementAddress = $userAPI->userAffiliate->placementAddress ?? NULL;
                    $user->placementFloor = $userAPI->userAffiliate->placementFloor ?? NULL;
                    // $user->type = $userAPI->type ?? NULL;
                    $user->save();
                }
                Auth::login($user);

                $listProduct = Product::where('campaign_id',$campaign_id)->limit(8)->get();
                $history = LuckywheelHistory::where('deleted', 0)->where('campaign_id',$campaign_id)->orderBy('created_at', 'DESC')->limit(10)->get();


                $dial = Dial::where('user_id',$user->id)->where('campaign_id', $campaign_id)->where('deleted', 0)->first();
                return view('luckywheel.index',compact('user','listProduct', 'history', 'dial', 'campaign'));
            } else {
                return redirect()->back()->withInput();
            }
        }
        return "Sự kiện không tồn tại!";
    }

    function getToken($request = []) {

        $data = [
            "UserName" => $request['username'],
            "Password" => $request['password'],
            "RememberMe" => "true",
            "LoginType" => "",
            "MacAddress" => "",
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.uto.vn/Users/Login');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        // $headers[] = 'Content-Type: multipart/form-data';
        $headers[] = 'accept: text/plain';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));


        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $temp =(array) json_decode($result);
        return $temp['result'];
    }

    function getInfoUser($token = "") {
        // $token = $this->getToken();

        $data = [
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.uto.vn/Users/GetUserInfo');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = array();
        $headers[] = 'accept: text/plain';
        $headers[] = 'Authorization: Bearer ' . $token;

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
}
