<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Dial;
use App\Models\User;
use Illuminate\Http\Request;

class DialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $items = Dial::where('deleted', 0)->get();
        return view('dial.index',compact('items'));
    }

    public function getCreate()
    {
        $campaigns = Campaign::where('deleted', 0)->get();
        $users = User::where('deleted', 0)->get();
        return view('dial.create', compact('campaigns', 'users'));
    }

    public function postCreate(Request $req)
    {
        $item = new Dial();
        $this->validate($req,
        [
            'user_id'=>'required',
            'campaign_id'=>'required',
            'turn'=>'numeric|min:0',
        ],);
        $item->user_id = $req->user_id;
        $item->campaign_id = $req->campaign_id;
        if (isset($req->turn))
        {
            if ($req->turn != "" && $req->turn >= 0) {
                $item->turn = $req->turn;
            }
        }
        $item->save();
        return redirect('dials')->with('thanhcong','Tạo thành công');
    }

    public function getUpdate($dial_id)
    {
        $dial = Dial::find($dial_id);
        return view('dial.update', compact('dial'));
    }

    public function postUpdate($dial_id, Request $req)
    {
        $item = Dial::find($dial_id);
        $this->validate($req,
        [
            'user_id'=>'required',
            'campaign_id'=>'required',
            'turn'=>'numeric|min:0',
        ],);
        $item->user_id = $req->user_id;
        $item->campaign_id = $req->campaign_id;
        if (isset($req->turn))
        {
            if ($req->turn != "" && $req->turn >= 0) {
                $item->turn = $req->turn;
            }
        }
        $item->save();
        return redirect('dials')->with('thanhcong','Lưu thành công');
    }

    public function getDel($dial_id )
    {
        $item = Dial::find($dial_id );
        $item->deleted = 1;
        $item->save();
        return redirect('dials')->with('thanhcong','Đã xóa thành công');
    }

    public function multipleCreate(Request $req)
    {
        $input = $req->all();
        $campaign_id = $input['campaign_id'];
        foreach ($input['data'] as $user_id) {
            $dial = Dial::where('user_id', $user_id)
            ->where('campaign_id', $campaign_id)
            ->where('deleted', '0')
            ->first();
            if($dial !="")
            {
                $dial->turn = $dial->turn + 1;
                $dial->save();
            }
            else
            {
                $dial = new Dial();
                $dial->user_id = $user_id;
                $dial->campaign_id = $campaign_id;
                $dial->save();
            }
        }
        return response()->json(['success'=> 'Tạo phiếu quay thành công']);
    }
}
