<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $items = Campaign::where('deleted', 0)->get();
        return view('campaign.index',compact('items'));
    }

    public function info($campaign_id)
    {
        $campaign = Campaign::find($campaign_id);
        return view('campaign.info', compact('campaign'));
    }

    public function getCreate()
    {
        return view('campaign.create');
    }

    public function postCreate(Request $req)
    {
        $this->validate($req,
        [
            'name'=>'required',
            'start'=>'date_format:Y-m-d\TH:i',
            'end'=>'date_format:Y-m-d\TH:i|after:start',
            'product1'=>'required',
            'quantity1'=>'numeric',
            'image1'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'product2'=>'required',
            'quantity2'=>'numeric',
            'image2'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'product3'=>'required',
            'quantity3'=>'numeric',
            'image3'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'product4'=>'required',
            'quantity4'=>'numeric',
            'image4'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'product5'=>'required',
            'quantity5'=>'numeric',
            'image5'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'product6'=>'required',
            'quantity6'=>'numeric',
            'image6'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'product7'=>'required',
            'quantity7'=>'numeric',
            'image7'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'product8'=>'required',
            'quantity8'=>'numeric',
            'image8'=>'required|mimes:jpg,jpeg,png,gif|max:2048',

        ],);
        try {
            DB::transaction(function() use ($req){
                $campaign = new Campaign();
                $campaign->name = $req->name;
                $campaign->start = $req->start;
                $campaign->end = $req->end;
                $campaign->save();

                //add sp
                $destinationPath = 'images/';

                $item1 = new Product();
                $item1->name = $req->product1;
                $item1->quantity = $req->quantity1;
                $item1->unit = $req->unit1;
                $item1->campaign_id = $campaign->id;
                $image1 = $req->file('image1');
                $getimage1 = ''.$image1->getClientOriginalName();
                $imagename1 = time().rand(1,100).'.'.pathinfo($getimage1, PATHINFO_EXTENSION);
                $image1->move($destinationPath, $imagename1);
                $item1->image = $imagename1;
                $item1->save();

                $item2 = new Product();
                $item2->name = $req->product2;
                $item2->quantity = $req->quantity2;
                $item2->unit = $req->unit2;
                $item2->campaign_id = $campaign->id;
                $image2 = $req->file('image2');
                $getimage2 = ''.$image2->getClientOriginalName();
                $imagename2 = time().rand(1,100).'.'.pathinfo($getimage2, PATHINFO_EXTENSION);
                $image2->move($destinationPath, $imagename2);
                $item2->image = $imagename2;
                $item2->save();

                $item3 = new Product();
                $item3->name = $req->product3;
                $item3->quantity = $req->quantity3;
                $item3->unit = $req->unit3;
                $item3->campaign_id = $campaign->id;
                $image3 = $req->file('image3');
                $getimage3 = ''.$image3->getClientOriginalName();
                $imagename3 = time().rand(1,100).'.'.pathinfo($getimage3, PATHINFO_EXTENSION);
                $image3->move($destinationPath, $imagename3);
                $item3->image = $imagename3;
                $item3->save();

                $item4 = new Product();
                $item4->name = $req->product4;
                $item4->quantity = $req->quantity4;
                $item4->unit = $req->unit4;
                $item4->campaign_id = $campaign->id;
                $image4 = $req->file('image4');
                $getimage4 = ''.$image4->getClientOriginalName();
                $imagename4 = time().rand(1,100).'.'.pathinfo($getimage4, PATHINFO_EXTENSION);
                $image4->move($destinationPath, $imagename4);
                $item4->image = $imagename4;
                $item4->save();

                $item5 = new Product();
                $item5->name = $req->product5;
                $item5->quantity = $req->quantity5;
                $item5->unit = $req->unit5;
                $item5->campaign_id = $campaign->id;
                $image5 = $req->file('image5');
                $getimage5 = ''.$image5->getClientOriginalName();
                $imagename5 = time().rand(1,100).'.'.pathinfo($getimage5, PATHINFO_EXTENSION);
                $image5->move($destinationPath, $imagename5);
                $item5->image = $imagename5;
                $item5->save();

                $item6 = new Product();
                $item6->name = $req->product6;
                $item6->quantity = $req->quantity6;
                $item6->unit = $req->unit6;
                $item6->campaign_id = $campaign->id;
                $image6 = $req->file('image6');
                $getimage6 = ''.$image6->getClientOriginalName();
                $imagename6 = time().rand(1,100).'.'.pathinfo($getimage6, PATHINFO_EXTENSION);
                $image6->move($destinationPath, $imagename6);
                $item6->image = $imagename6;
                $item6->save();

                $item7 = new Product();
                $item7->name = $req->product7;
                $item7->quantity = $req->quantity7;
                $item7->unit = $req->unit7;
                $item7->campaign_id = $campaign->id;
                $image7 = $req->file('image7');
                $getimage7 = ''.$image7->getClientOriginalName();
                $imagename7 = time().rand(1,100).'.'.pathinfo($getimage7, PATHINFO_EXTENSION);
                $image7->move($destinationPath, $imagename7);
                $item7->image = $imagename7;
                $item7->save();

                $item8 = new Product();
                $item8->name = $req->product8;
                $item8->quantity = $req->quantity8;
                $item8->unit = $req->unit8;
                $item8->campaign_id = $campaign->id;
                $image8 = $req->file('image8');
                $getimage8 = ''.$image8->getClientOriginalName();
                $imagename8 = time().rand(1,100).'.'.pathinfo($getimage8, PATHINFO_EXTENSION);
                $image8->move($destinationPath, $imagename8);
                $item8->image = $imagename8;
                $item8->save();
            });

        }
        catch(\Exception $e) {
            DB::rollback();
            return redirect('campaigns')->with('thatbai','Lỗi: '.$e);
        }
        return redirect('campaigns')->with('thanhcong','Tạo thành công');
    }

    public function getUpdate($campaign_id)
    {
        $campaign = Campaign::find($campaign_id);
        return view('campaign.update', compact('campaign'));
    }

    public function postUpdate($campaign_id, Request $req)
    {
        $item = Campaign::find($campaign_id);
        $this->validate($req,
        [
            'name'=>'required',
            'start'=>'date_format:Y-m-d\TH:i',
            'end'=>'date_format:Y-m-d\TH:i|after:start',
        ],);
        $item->name = $req->name;
        $item->start = $req->start;
        $item->end = $req->end;
        $item->save();
        return redirect('campaigns')->with('thanhcong','Lưu thành công');
    }

    public function getDel($campaign_id )
    {
        $item = Campaign::find($campaign_id );
        $item->deleted = 1;
        $item->save();
        return redirect('campaigns')->with('thanhcong','Đã xóa thành công');
    }
}
