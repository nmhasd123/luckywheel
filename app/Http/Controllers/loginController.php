<?php

namespace App\Http\Controllers;

use App\Models\Dial;
use App\Models\LuckywheelHistory;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class loginController extends Controller
{
    public function username()
    {
        return 'userName';
    }
    public function login(Request $request)
    {
        $credentials = [
            'userName' => $request['username'],
            'password' => $request['password'],
        ];
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $listProduct = Product::all();
            $history = LuckywheelHistory::all();
            // $status = LuckywheelHistory::where('name', '=', Auth::user()->fullName)
            //     ->where('phone', '=', Auth::user()->userContact->phoneNumber)
            //     ->count();
            $status = 0;
            //mới thêm
            // $campaign_id = $request['campaign_id'];
            // $dial = Dial::where('user_id',$user->id)->where('campaign_id', $campaign_id)->first();
            return view('luckywheel.index',compact('user','listProduct', 'history', 'status'));
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function getLogin()
    {
        return view('login.login');
    }

    public function getLoginAPI()
    {
        return view('login.loginapi');
    }
    public function postLoginAPI(Request $request)
    {
        $token = $this->getToken($request);
        if( $token != null && $token != "")
        {
            $userAPI = json_decode($this->getInfoUser($token))->result;

            // $user->userName;
            // $user->fullName;
            // $user->active;
            // $user->id;
            $user = User::where('id_server', $userAPI->id)->first();
            if($user == NULL || $user == "")
            {
                $user = new User();
                $user->id_server = $userAPI->id;
                $user->email = $userAPI->userContact->email ?? NULL;
                $user->phoneNumber = $userAPI->phoneNumber ?? NULL;
                $user->fullName = $userAPI->fullName ?? "";
                $user->userName = $userAPI->userName ?? NULL;
                $user->sponsor = $userAPI->userAffiliate->sponsor ?? NULL;
                $user->sponsorId = $userAPI->userAffiliate->sponsorId ?? NULL;
                $user->sponsorAddress = $userAPI->userAffiliate->sponsorAddress ?? NULL;
                $user->sponsorFloor = $userAPI->userAffiliate->sponsorFloor ?? NULL;
                $user->placement = $userAPI->userAffiliate->placement ?? NULL;
                $user->placementId = $userAPI->userAffiliate->placementId ?? NULL;
                $user->placementAddress = $userAPI->userAffiliate->placementAddress ?? NULL;
                $user->placementFloor = $userAPI->userAffiliate->placementFloor ?? NULL;
                // $user->type = $userAPI->type ?? NULL;
                $user->save();
            }

            // return $user;

            $listProduct = Product::all();
            $history = LuckywheelHistory::all();
            $status = LuckywheelHistory::where('name', '=', $user->fullName)
                ->where('phone', '=', $user->userContact->phoneNumber)
                ->count();
            return view('luckywheel.index2',compact('user','listProduct', 'history', 'status'));
        }
        else
        {
            return view('login.loginapi');
        }
    }

    function getToken($request = []) {

        $data = [
            "UserName" => $request['username'],
            "Password" => $request['password'],
            "RememberMe" => "true",
            "LoginType" => "",
            "MacAddress" => "",
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.uto.vn/Users/Login');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        // $headers[] = 'Content-Type: multipart/form-data';
        $headers[] = 'accept: text/plain';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));


        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $temp =(array) json_decode($result);
        return $temp['result'];
    }

    function getInfoUser($token = "") {
        // $token = $this->getToken();

        $data = [
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.uto.vn/Users/GetUserInfo');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = array();
        $headers[] = 'accept: text/plain';
        $headers[] = 'Authorization: Bearer ' . $token;

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
    // public function login(Request $request)
    // {
    //     //dd(Auth::check());
    //     if (Auth::check()) {
    //         return redirect()->route('home.index');
    //     }

    //     $token = Request()->get('token', '');
    //     if ($token != '') {
    //         $checkLoginByToken = $this->checkLoginByToken($request);
    //         if ($checkLoginByToken) {
    //             return redirect($checkLoginByToken);
    //         }
    //     }


    //     $data = [
    //         'meta_title' => 'Đăng nhập'
    //     ];

    //     if ($request->getMethod() == 'POST') {
    //         $ref = $request->get('ref');

    //         // "UserName" => 'leoodz',
    //         // "Password" => 'hung12300',
    //         $params = [
    //             'userName'   => 'leoodz',
    //             'password'   => 'hung12300',
    //             'rememberMe' => 'true'
    //         ];

    //         $res = $this->apiGetLogin($params);


    //         if (!empty($res['result'])) {
    //             // Lấy thông tin thành viên
    //             //dd($res); exit;

    //             $token = $res['result'];
    //             $infoUsers = $this->apiGetInfoUsers($res['result']);

    //             if (!empty($infoUsers['result'])) {
    //                 $data = $infoUsers['result'];

    //                 $users = User::where('username', $data['userName'])->first();
    //                 if ($users) {
    //                     $users->update([
    //                         'uto_id'   => $data['id'],
    //                         'username' => $data['userName'],
    //                         'name' => $data['fullName'],
    //                         'password' => Hash::make($data['password']),
    //                         'avatar' => $data['avatar']
    //                     ]);
    //                 } else {
    //                     $users = User::create([
    //                         'uto_id'   => $data['id'],
    //                         'username' => $data['userName'],
    //                         'name' => $data['fullName'],
    //                         'password' => Hash::make($data['password']),
    //                         'avatar' => $data['avatar']
    //                     ]);
    //                 }

    //                 if (!$users) {
    //                     $request->session()->flash('msg', 'Tài khoản hoặc mật khẩu không chính xác.');
    //                     return redirect(Route('login'));
    //                 }

    //                 $credentials = [
    //                     'username' => $users->username,
    //                     'password' => $data['password']
    //                 ];

    //                 //dd($credentials);

    //                 if (Auth::attempt($credentials, true)) {
    //                     $request->session()->put('UTO_TOKEN', $token);
    //                     if ($ref != '') {
    //                         return redirect($ref);
    //                     } else {
    //                         return redirect(Route('home.index'));
    //                     }
    //                 }
    //             } else {
    //                 $request->session()->flash('msg', 'Tài khoản hoặc mật khẩu không chính xác.');
    //                 return redirect(Route('login'));
    //             }
    //         } else {
    //             $request->session()->flash('msg', 'Tài khoản hoặc mật khẩu không chính xác.');
    //             return redirect(Route('login'));
    //         }
    //     }

    //     return view('login', $data);
    // }

    // public function apiGetLogin($params) {
    //     $client = new \GuzzleHttp\Client();
    //     $url = config('constants.api') . 'Users/Login';

    //     $res = $client->request('POST', $url, [
    //         'form_params'    => $params,
    //         'verify'  => false
    //     ]);
    //     $data = json_decode($res->getBody(), true);
    //     return $data;
    // }

    // public function apiGetInfoUsers($token) {
    //     $client = new \GuzzleHttp\Client();
    //     $url = config('constants.api') . 'Users/GetUserInfo';

    //     $headers = [
    //         'Authorization' => 'Bearer ' . $token
    //     ];

    //     $res = $client->request('GET', $url, [
    //         'headers' => $headers,
    //         'verify'  => false
    //     ]);
    //     $data = json_decode($res->getBody(), true);
    //     return $data;
    // }

}
