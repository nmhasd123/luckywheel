<?php

use App\Http\Controllers\CampaignController;
use App\Http\Controllers\DialController;
use App\Http\Controllers\HistoryLuckywheelController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\loginController;
use App\Http\Controllers\Luckywheel;
use App\Http\Controllers\UserController;
use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', [Luckywheel::class, 'getList']);

Route::get('/gettoken', [loginController::class, 'getInfoUser']);
Route::get('/logout', function(){
    Auth::logout();
    return redirect('/');
});
Route::get('/login', [loginController::class, 'getLogin']);
// Route::post('/login', [loginController::class, 'postLogin']);
Route::post('/login', [loginController::class, 'login']);
Route::get('/loginapi', [loginController::class, 'getLoginAPI']);
Route::post('/loginapi', [loginController::class, 'postLoginAPI']);

Route::get('/users', [UserController::class, 'index'])->name('admin.dashboard');
Route::get('/user-create', [UserController::class, 'getCreate']);
Route::post('/user-create', [UserController::class, 'postCreate']);
Route::get('/user-update/{id}', [UserController::class, 'getUpdate']);
Route::post('/user-update/{id}', [UserController::class, 'postUpdate']);
Route::get('/user-del/{id}', [UserController::class, 'getDel']);

Route::get('/products', [ProductController::class, 'index']);
Route::get('/product-create', [ProductController::class, 'getCreate']);
Route::post('/product-create', [ProductController::class, 'postCreate']);
Route::get('/product-update/{id}', [ProductController::class, 'getUpdate']);
Route::post('/product-update/{id}', [ProductController::class, 'postUpdate']);
Route::get('/product-del/{id}', [ProductController::class, 'getDel']);

Route::get('/dials', [DialController::class, 'index']);
Route::get('/dial-create', [DialController::class, 'getCreate']);
Route::post('/dial-create', [DialController::class, 'postCreate']);
Route::get('/dial-update/{id}', [DialController::class, 'getUpdate']);
Route::post('/dial-update/{id}', [DialController::class, 'postUpdate']);
Route::get('/dial-del/{id}', [DialController::class, 'getDel']);
Route::post('/dial-multi-create}', [DialController::class, 'multipleCreate'])->name('dial-multi-create');

Route::get('/campaigns', [CampaignController::class, 'index'])->name('campaign');
Route::get('/campaign-info/{id}', [CampaignController::class, 'info']);
Route::get('/campaign-create', [CampaignController::class, 'getCreate']);
Route::post('/campaign-create', [CampaignController::class, 'postCreate']);
Route::get('/campaign-update/{id}', [CampaignController::class, 'getUpdate']);
Route::post('/campaign-update/{id}', [CampaignController::class, 'postUpdate']);
Route::get('/campaign-del/{id}', [CampaignController::class, 'getDel']);

Route::get('/list-history', [HistoryLuckywheelController::class, 'index']);
Route::post('/history-create', [HistoryLuckywheelController::class, 'postCreate']);
Route::get('/{campaign_id}', [Luckywheel::class, 'getLogin']);
Route::post('/{campaign_id}', [Luckywheel::class, 'login'])->name('luckywheel');

// Route::post('/postData',function (Request $request) {
//     return response()->json(['success'=> 'cập nhập thành công']);;
// })->name('ajax.postData');

Route::get('/test', function () {
    return view('test');
});

Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('login');
Route::prefix('admin')->group(function() {
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'Auth\AdminLoginController@logout')->name('admin.logout');
    // Route::get('/', 'Auth\AdminController@index')->name('admin.dashboard');
   }) ;

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
