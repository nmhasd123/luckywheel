<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>UTO-luckywheel</title>
    <link rel="stylesheet" href="{{asset('css/typo/typo.css')}}" />
    <link rel="stylesheet" href="{{asset('css/hc-canvas-luckwheel.css')}}" />
    <script language="javascript" src="//code.jquery.com/jquery-2.0.0.min.js"></script>
    <style>
        body{
            overflow    : hidden ;
        }
    </style>
  </head>
  <body class="bg">

    <div class="container">
      <div class="row">
        <!-- logo -->
        <div class="col-xl-12">
          <div class="logo">
            <img src="{{asset('images/logo.png')}}" alt="">
          </div>
        </div>
        <!-- infor -->
        <div class="col-xl-6 full-height center-align">
          <div class="row-cus">

            <!-- user infor -->
            <div class="user-infor">
              <div class="title">
                @if (isset($dial))
                    @if ($dial->turn > 0)
                        CHÚC MỪNG ! BẠN CÓ <a id="turn">{{$dial->turn}}</a> LƯỢT QUAY.
                    @else
                        BẠN ĐÃ HẾT LƯỢT QUAY!
                    @endif
                @else
                    BẠN CHƯA ĐỦ ĐIỀU KIỆN THAM GIA SỰ KIỆN!
                @endif


              </div>
              <div class="user-infor-row">
                <div class="user-infor-row_label">
                  Số điện thoại:
                </div>
                <div class="user-infor-row_value">
                  {{$user->phoneNumber}}
                </div>
              </div>
              <div class="user-infor-row">
                <div class="user-infor-row_label">
                  Họ và tên:
                </div>
                <div class="user-infor-row_value">
                    {{$user->fullName}}
                </div>
              </div>
            </div>

            <!-- history list-->
            <div class="history">
              <div class="title">
                DANH SÁCH TRÚNG THƯỞNG SỰ KIỆN {{$dial->campaign->name ?? ""}}
              </div>
              <div class="history-content">
                <table>
                    @foreach ($history as $row)
                    <tr>
                        <td>{{$row->name}}</td>
                        <td>{{$row->phone}}</td>
                        <td>{{$row->product}}</td>
                      </tr>
                    @endforeach

                </table>
              </div>
            </div>

          </div>
        </div>

        <!-- spinner -->
        <div class="col-xl-6 full-height center-align">
          <div class="wrapper typo" id="wrapper">
            <section id="luckywheel" class="hc-luckywheel">
              <div class="hc-luckywheel-container">
                <canvas class="hc-luckywheel-canvas" width="500px" height="500px">Vòng Xoay May Mắn</canvas>
              </div>
              <a class="hc-luckywheel-btn" href="javascript:;">Quay</a>
            </section>
          </div>
        </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{asset('js/hc-canvas-luckwheel.js')}}"></script>
    <script>
      var isPercentage = true;
      var prizes = [
        @foreach ($listProduct as $product)
            {
                id: "{{$product->id}}",
                text: "{{$product->name}}",
                img: "{{asset('images')}}/{{$product->image}}",
                number: {{$product->quantity ?? 0}}, // 1%,
                percentpage: 0.125 // 12.5%
            },
          @endforeach
            ];
      document.addEventListener(
        "DOMContentLoaded",
        function() {

            turn = {{$dial->turn ?? "0"}};
          hcLuckywheel.init({
            id: "luckywheel",
            config: function(callback) {
              callback &&
                callback(prizes);
            },
            mode : "both",
            getPrize: function(callback) {
              var rand = randomIndex(prizes);
                if ( rand === null) {
                    Swal.fire(
                            'Chương trình kết thúc',
                            'Đã hết phần thưởng',
                            'error'
                            )
                }
                else {
                    let product_id = prizes[rand]['id'];
                    let user_id = "{{$user->id}}";
                    let dial_id = "{{$dial->id ?? ''}}";
                    let campaign_id = "{{$campaign->id}}";
                    let _token   = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        url : "{{asset('history-create')}}",
                        type : "post",
                        dataType:"text",
                        data : {
                            user_id : user_id,
                            product_id : product_id,
                            dial_id : dial_id,
                            campaign_id : campaign_id,
                            _token: _token
                        },
                        success : function (result){
                            if(result == '0')
                            {
                                var chances = rand;
                                callback && callback([rand, chances]);
                            }
                            else if(result == '1'){
                                Swal.fire(
                                    'Bạn đã hết lượt quay!',
                                    '',
                                    'error'
                                )
                            }
                            else if(result == '2'){
                                Swal.fire(
                                    'Sự kiện chưa bắt đầu!',
                                    '',
                                    'error'
                                )
                            }
                            else if(result == '3'){
                                Swal.fire(
                                    'Sự kiện đã kết thúc!',
                                    '',
                                    'error'
                                )
                            }
                            else if(result == '-1'){
                                Swal.fire(
                                    'Bạn chưa đủ điều kiện tham gia sự kiện!',
                                    '',
                                    'error'
                                )
                            }
                            else {
                                Swal.fire(
                                'Chương trình kết thúc',
                                'Đã hết phần thưởng',
                                'error'
                                )
                            }
                        }
                    });
                }
            },
            gotBack: function(data) {
              if(data == null){
                Swal.fire(
                  'Chương trình kết thúc',
                  'Đã hết phần thưởng',
                  'error'
                )
              } else if (data == 'Chúc bạn may mắn lần sau'){
                Swal.fire(
                  'Bạn không trúng thưởng',
                  data,
                  'error'
                )
              } else{
                Swal.fire(
                  'Đã trúng giải',
                  data,
                  'success'
                ).then(function(){
                    location.reload();
                });
                turn = turn -1;
                $('#turn').html(turn);
              }
            }
          });
        },
        false
      );
      function randomIndex(prizes){
        if(isPercentage){
          var counter = 1;
          for (let i = 0; i < prizes.length; i++) {
            if(prizes[i].number == 0){
              counter++
            }
          }
          if(counter == prizes.length + 1){
            return null
          }
          let rand = Math.random();
          let prizeIndex = null;

          // console.log(rand);

          switch (true) {
            case rand < prizes[7].percentpage:
              prizeIndex = 7;
            break;
            case rand < prizes[7].percentpage + prizes[6].percentpage:
              prizeIndex = 6;
            break;
            case rand < prizes[7].percentpage + prizes[6].percentpage + prizes[5].percentpage:
              prizeIndex = 5;
              break;
            case rand < prizes[7].percentpage + prizes[6].percentpage + prizes[5].percentpage + prizes[4].percentpage:
              prizeIndex = 4 ;
              break;
            case rand < prizes[7].percentpage + prizes[6].percentpage + prizes[5].percentpage + prizes[4].percentpage + prizes[3].percentpage:
              prizeIndex = 3;
              break;
            case rand < prizes[7].percentpage + prizes[6].percentpage + prizes[5].percentpage + prizes[4].percentpage + prizes[3].percentpage + prizes[2].percentpage:
              prizeIndex = 2;
              break;
            case rand < prizes[7].percentpage + prizes[6].percentpage + prizes[5].percentpage + prizes[4].percentpage + prizes[3].percentpage + prizes[2].percentpage + prizes[1].percentpage:
              prizeIndex = 1;
              break;
            case rand < prizes[7].percentpage + prizes[6].percentpage + prizes[5].percentpage + prizes[4].percentpage + prizes[3].percentpage + prizes[2].percentpage + prizes[1].percentpage  + prizes[0].percentpage:
              prizeIndex = 0;
              break;
          }

        //   console.log(prizes[prizeIndex])

          if(prizes[prizeIndex].number != 0){
            prizes[prizeIndex].number = prizes[prizeIndex].number - 1
            return prizeIndex
          }else{
            return randomIndex(prizes)
          }
        }else{
          var counter = 0;
          for (let i = 0; i < prizes.length; i++) {
            if(prizes[i].number == 0){
              counter++
            }
          }
          if(counter == prizes.length){
            return null
          }
          var rand = (Math.random() * (prizes.length)) >>> 0;
          if(prizes[rand].number != 0){
            prizes[rand].number = prizes[rand].number - 1
            return rand
          }else{
            return randomIndex(prizes)
          }
        }
      }
    </script>
  </body>
</html>
