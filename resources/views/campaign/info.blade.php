 @extends('main')
 @section('content')
     <div id="content">
         <!-- Begin Page Content -->
         <div class="container-fluid">

             <!-- Page Heading -->
             <h1 class="h3 mb-2 text-gray-800">Chiến dịch: {{ $campaign->name }}</h1>

             <!-- DataTales Example -->
             <div class="card shadow mb-4">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary"></h6>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">

                         <div class="container">
                             <div class="row">
                                 <div class="col-lg-12 mb-5 mb-lg-0">

                                     <div class="row form-group">
                                         <div class="col-md-12">
                                             <label class="text-black">Tên chiến dịch</label>
                                             <input type="text" name="name" value="{{ $campaign->name }}"
                                                 class="form-control" disabled>
                                         </div>
                                     </div>

                                     <div class="row form-group">
                                         <div class="col-md-6">
                                             <label class="text-black" for="subject">Bắt đầu</label>
                                             <input type="datetime-local" name="start"
                                                 value="{{ date('Y-m-d\TH:i', strtotime($campaign->start)) }}"
                                                 class="form-control" disabled>
                                         </div>

                                         <div class="col-md-6">
                                             <label class="text-black" for="subject">Kết thúc</label>
                                             <input type="datetime-local" name="end"
                                                 value="{{ date('Y-m-d\TH:i', strtotime($campaign->end)) }}"
                                                 class="form-control" disabled>
                                         </div>
                                     </div>

                                 </div>
                             </div>
                         </div>

                     </div>
                     <div class="border-bottom-primary"></div>
                 </div>
                 <div class="card-body">
                     <h3>Danh sách phần thưởng</h3>
                     <div class="table-responsive">
                         <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                             <thead>
                                 <tr>
                                     <th>Image</th>
                                     <th>Name</th>
                                     <th>Quantity</th>
                                     <th>Unit</th>
                                     <th>Action</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 @foreach ($campaign->products as $product)
                                     <tr>
                                         <td>
                                             @if (isset($product->image))
                                                 <img src="{{ asset('images/' . $product->image) }}" alt="{{ $product->image }}" style="width: 150px; height: 150px;">
                                             @endif
                                         </td>
                                         <td>{{ $product->name }}</td>
                                         <td>{{ $product->quantity }}</td>
                                         <td>{{ $product->unit }}</td>
                                         <td>
                                             <a href="{{ asset('product-update') . "/" . $product->id }}" class="btn btn-warning btn-circle">
                                                 <i class="fas fa-pencil-alt"></i>
                                             </a>
                                         </td>
                                     </tr>
                                 @endforeach
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>

         </div>
         <!-- /.container-fluid -->
     </div>
 @endsection
