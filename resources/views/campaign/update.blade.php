 @extends('main')
 @section('content')
     <div id="content">
         <!-- Begin Page Content -->
         <div class="container-fluid">

             <!-- Page Heading -->
             <h1 class="h3 mb-2 text-gray-800">Chiến dịch</h1>

             <!-- DataTales Example -->
             <div class="card shadow mb-4">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary"></h6>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">

                         <div class="container">
                             <div class="row">
                                 <div class="col-lg-2 mb-5 mb-lg-0"></div>
                                 <div class="col-lg-8 mb-5 mb-lg-0">

                                     <form action="{{ asset('campaign-update/' . $campaign->id) }}" method="POST"
                                         enctype="multipart/form-data">
                                         {{ csrf_field() }}

                                         <div class="row form-group">
                                            <div class="col-md-12">
                                                <label class="text-black">Tên chiến dịch</label>
                                                <input type="text" name="name" value="{{$campaign->name}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="text-black" for="subject">Bắt đầu</label>
                                                <input type="datetime-local" name="start" value="{{date('Y-m-d\TH:i', strtotime($campaign->start))}}" class="form-control">
                                            </div>

                                            <div class="col-md-6">
                                                <label class="text-black" for="subject">Kết thúc</label>
                                                <input type="datetime-local" name="end" value="{{date('Y-m-d\TH:i', strtotime($campaign->end))}}" class="form-control">
                                            </div>
                                        </div>

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <input type="submit" value="Save"
                                                     class="btn btn-primary btn-md text-white">
                                             </div>
                                         </div>
                                     </form>
                                 </div>
                             </div>
                         </div>

                     </div>
                 </div>
             </div>

         </div>
         <!-- /.container-fluid -->
     </div>
 @endsection
