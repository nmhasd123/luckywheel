 @extends('main')
 @section('content')
 <div id="content">
 	<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Chiến dịch</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                    <div class="container">
                        <div class="row">
                        <div class="col-lg-2 mb-5 mb-lg-0"></div>
                        <div class="col-lg-8 mb-5 mb-lg-0">

                            <form action="{{asset('campaign-create')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                {{-- <div>
                                    <input id="rangeInput" type="range" min="0" max="200" oninput="amount.value=rangeInput.value" />
                                    <input id="amount" type="number" value="100" min="0" max="200" oninput="rangeInput.value=amount.value" />
                                </div> --}}

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label class="text-black">Tên chiến dịch</label>
                                        <input type="text" name="name" value="" class="form-control">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <label class="text-black" for="subject">Thời gian bắt đầu</label>
                                        <input type="datetime-local" name="start" value="" class="form-control">
                                    </div>

                                    <div class="col-md-6">
                                        <label class="text-black" for="subject">Thời gian kết thúc</label>
                                        <input type="datetime-local" name="end" value="" class="form-control">
                                    </div>
                                </div>
                                @for ($i = 1; $i <= 8; $i++)
                                    <div class="border-bottom-primary"><h3>Phần thưởng {{$i}}</h3></div>

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                        <label class="text-black" for="subject">tên phần thưởng</label>
                                        <input type="text" name="product{{$i}}" value="" class="form-control" required>
                                        </div>
                                        <div class="col-md-6">
                                        <label class="text-black">Hình ảnh</label>
                                        <input type="file" name="image{{$i}}" multiple  class="">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class="text-black" for="subject">Tỉ lệ</label>
                                            <input type="number" id="percent{{$i}}" name="percent{{$i}}" value="{{$i == 8 ? 100 : 0}}" class="form-control percent" required min="0" max="100">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class="text-black" for="subject">Số lượng</label>
                                            <input type="number" name="quantity{{$i}}" value="" class="form-control">
                                        </div>

                                        <div class="col-md-6">
                                            <label class="text-black" for="subject">Đơn vị</label>
                                            <input type="text" name="unit{{$i}}" value="" class="form-control">
                                        </div>
                                    </div>
                                @endfor

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <input type="submit" value="Save" class="btn btn-primary btn-md text-white">
                                    </div>
                                </div>

                            </form>
                        </div>
                        </div>
                    </div>

              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
 </div>
 @endsection
 @section('scripts')
    <script>
        $('.percent').change(function () {
            var a = $(this).val();
            var b = $('#percent8').val() - a;
            if(b < 0)
            {
                alert("quá 100% rồi");
            }
            else
            $('#percent8').val(b);
        });
    </script>
 @endsection
