 @extends('main')
 @section('content')
     <div id="content">
         <!-- Begin Page Content -->
         <div class="container-fluid">

             <!-- Page Heading -->
             <h1 class="h3 mb-2 text-gray-800">Product</h1>

             <!-- DataTales Example -->
             <div class="card shadow mb-4">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary"></h6>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">

                         <div class="container">
                             <div class="row">
                                 <div class="col-lg-2 mb-5 mb-lg-0"></div>
                                 <div class="col-lg-8 mb-5 mb-lg-0">

                                     <form action="{{ asset('product-update/' . $product->id) }}" method="POST"
                                         enctype="multipart/form-data">
                                         {{ csrf_field() }}

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <label class="text-black" for="subject">Name</label>
                                                 <input type="text" name="name" value="{{ $product->name }}"
                                                     class="form-control" required>
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <label class="text-black">Image</label>
                                                 <img src="{{asset('images/' . $product->image) }}"
                                                     alt="{{$product->image }}" style="width: 250px; height: 250px;">
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <label class="text-black">Add image</label>
                                                 <input type="file" name="image" multiple class="">
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <label class="text-black" for="subject">Quantity</label>
                                                 <input type="number" name="quantity" value="{{ $product->quantity }}"
                                                     class="form-control">
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <label class="text-black" for="subject">Unit </label>
                                                 <input type="text" name="unit" value="{{ $product->unit }}"
                                                     class="form-control">
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <input type="submit" value="Save"
                                                     class="btn btn-primary btn-md text-white">
                                             </div>
                                         </div>


                                     </form>
                                 </div>
                             </div>
                         </div>

                     </div>
                 </div>
             </div>

         </div>
         <!-- /.container-fluid -->
     </div>
 @endsection
