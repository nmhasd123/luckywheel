 @extends('main')
 @section('content')
     <div id="content">
         <!-- Begin Page Content -->
         <div class="container-fluid">

             <!-- Page Heading -->
             <h1 class="h3 mb-2 text-gray-800">Phiếu quay</h1>

             <!-- DataTales Example -->
             <div class="card shadow mb-4">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary"></h6>
                     <a href="{{ 'dial-create' }}" class="btn btn-info">
                         <i class="fas fa-pencil-alt"></i> create</a>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">
                         <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                             <thead>
                                 <tr>
                                     <th>User</th>
                                     <th>Chiến dịch</th>
                                     <th>Số lần</th>
                                     <th>Action</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 @foreach ($items as $item)
                                     <tr>
                                         <td>{{ $item->user->fullName ?? "" }}</td>
                                         <td>{{ $item->campaign->name ?? "" }}</td>
                                         <td>{{ $item->turn }}</td>
                                         <td>
                                             <a href="{{ 'dial-update/' . $item->id }}" class="btn btn-info btn-circle">
                                                 <i class="fas fa-info-circle"></i>
                                             </a>
                                             <a href="{{ 'dial-del/' . $item->id }}" class="btn btn-danger btn-circle">
                                                 <i class="fas fa-trash"></i>
                                             </a>
                                         </td>
                                     </tr>
                                 @endforeach
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>

         </div>
         <!-- /.container-fluid -->
     </div>
 @endsection
