 @extends('main')
 @section('content')
     <div id="content">
         <!-- Begin Page Content -->
         <div class="container-fluid">

             <!-- Page Heading -->
             <h1 class="h3 mb-2 text-gray-800">Phiếu quay</h1>

             <!-- DataTales Example -->
             <div class="card shadow mb-4">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary"></h6>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">

                         <div class="container">
                             <div class="row">
                                 <div class="col-lg-2 mb-5 mb-lg-0"></div>
                                 <div class="col-lg-8 mb-5 mb-lg-0">

                                     <form action="{{ asset('dial-create') }}" method="POST" enctype="multipart/form-data">
                                         {{ csrf_field() }}

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <label class="text-black" for="subject">User id</label>
                                                 {{-- <input type="text" name="user_id" value="" class="form-control" required> --}}
                                                 <select name="user_id" class="form-control">
                                                     @foreach ($users as $user)
                                                         <option value="{{ $user->id }}">{{ $user->fullName }}</option>
                                                     @endforeach
                                                 </select>
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col-md-6">
                                                 <label class="text-black" for="subject">Chiến dịch</label>
                                                 <select name="campaign_id" class="form-control">
                                                     @foreach ($campaigns as $campaign)
                                                         <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                                                     @endforeach
                                                 </select>
                                                 {{-- <input type="text" name="campaign_id" value="" class="form-control"> --}}
                                             </div>

                                             <div class="col-md-6">
                                                 <label class="text-black" for="subject">Số lượt</label>
                                                 <input type="number" name="turn" value="" class="form-control">
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col-md-12">
                                                 <input type="submit" value="Save"
                                                     class="btn btn-primary btn-md text-white">
                                             </div>
                                         </div>

                                     </form>
                                 </div>
                             </div>
                         </div>

                     </div>
                 </div>
             </div>

         </div>
         <!-- /.container-fluid -->
     </div>
 @endsection
