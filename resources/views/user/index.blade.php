@extends('main')
@section('styles')
    <link href="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">User</h1>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"></h6>
                    <a href="{{ 'user-create' }}" class="btn btn-info">
                        <i class="fas fa-pencil-alt"></i> create</a>
                </div>
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">

                        <div class="row form-group">
                            <div class="col-md-3">
                                <button id="send" class="btn btn-warning">send</button>
                            </div>
                            <div class="col-md-9 row">
                                <div class="col-md-4">
                                    <label class="text-black" for="subject">Chiến dịch</label>
                                </div>
                                <div class="col-md-8">
                                    <select id="campaign_id" name="campaign_id" class="form-control">
                                        @foreach ($campaigns as $campaign)
                                            <option value="{{$campaign->id}}">{{$campaign->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>User name</th>
                                    <th>Full name</th>
                                    <th>Phone</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td><input class="user_id" type="checkbox"  name="user_id" value="{{$item->id}}"></td>
                                        <td>{{ $item->userName }}</td>
                                        <td>{{ $item->fullName }}</td>
                                        <td>{{ $item->phoneNumber }}</td>
                                        <td>
                                            <a href="{{ 'user-update/' . $item->id }}" class="btn btn-info btn-circle">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                            <a href="{{ 'user-del/' . $item->id }}" class="btn btn-danger btn-circle">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->
    </div>
@endsection
@section('scripts')
    <script src="{{asset('admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script><script>
        $(document).ready(function() {
            $('#dataTable').DataTable(
                {
                    scrollY:        300,
                    scrollX:        true,
                    scrollCollapse: true,
                    paging:         false,
                    fixedColumns:   {
                        leftColumns: 2
                    },
                    columnDefs: [ {
                        orderable: false,
                        className: 'select-checkbox',
                        blurable: true,
                        targets:   0
                    } ],
                    select: {
                        style:    'os',
                        selector: 'td:first-child'
                    },
                    order: [[ 1, 'asc' ]]
                }
            );

            $( "#send" ).click(function() {
                data = Array();
                var campaign = $('#campaign_id').val();
                $("input:checkbox[name=user_id]:checked").each(function() {
                    data.push($(this).val());
                });
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('dial-multi-create')}}",
                    type: "post",
                    data: {
                        data: data,
                        campaign_id: campaign
                    },
                    success: function(data) {
                        alert(data.success);
                        $(".user_id").prop("checked", false);
                        // var table = $('.dataTable').DataTable();
                        // table.ajax.reload();
                    }
                });
            });
        });
    </script>
@endsection
