 @extends('main')
 @section('content')
 <div id="content">
 	<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">User</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                    <div class="container">
                        <div class="row">
                        <div class="col-lg-2 mb-5 mb-lg-0"></div>
                        <div class="col-lg-8 mb-5 mb-lg-0">

                            <form action="{{asset('user-create')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">User name</label>
                                    <input type="text" name="userName" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">Full name</label>
                                    <input type="text" name="fullName" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">Phone number</label>
                                    <input type="text" name="phoneNumber" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">Email</label>
                                    <input type="text" name="email" value="" class="form-control" >
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">avatar</label>
                                    <input type="file" name="avatar" value="" class="form-control" >
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">sponsor</label>
                                    <input type="text" name="sponsor" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">sponsorId</label>
                                    <input type="text" name="sponsorId" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">sponsorAddress</label>
                                    <input type="text" name="sponsorAddress" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">sponsorFloor</label>
                                    <input type="number" name="sponsorFloor" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">active</label>
                                    <select name="active" id="">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                    </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">placement</label>
                                    <input type="text" name="placement" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">placementId</label>
                                    <input type="text" name="placementId" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">placementAddress</label>
                                    <input type="text" name="placementAddress" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <label class="text-black" for="subject">placementFloor</label>
                                    <input type="number" name="placementFloor" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label class="text-black" for="subject">type</label>
                                        <select name="type">
                                            <option value="f0">f0</option>
                                            <option value="f1">f1</option>
                                            <option value="f2">f2</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <input type="submit" value="Save" class="btn btn-primary btn-md text-white">
                                    </div>
                                </div>


                            </form>
                        </div>
                        </div>
                    </div>

              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
 </div>
 @endsection
