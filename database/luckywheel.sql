-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th4 14, 2022 lúc 11:46 AM
-- Phiên bản máy phục vụ: 5.7.33-log
-- Phiên bản PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `luckywheel`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$WqTNtjkdNOR71zccDXwWpuBsn1rh9yekU4Mo3bGBMtVR0v60wM9Oi', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `campaigns`
--

CREATE TABLE `campaigns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `campaigns`
--

INSERT INTO `campaigns` (`id`, `name`, `start`, `end`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Sự kiện 1', '2022-04-07 11:43:00', '2022-04-08 11:43:00', 0, '2022-04-06 21:43:41', '2022-04-06 21:43:41'),
(2, 'chiến dịch 2', '2022-04-07 15:52:00', '2022-04-08 15:52:00', 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(3, 'Chiến dịch 3', '2022-04-07 16:10:00', '2022-04-09 16:10:00', 0, '2022-04-07 02:43:36', '2022-04-07 02:43:53'),
(4, 'chiến dịch 4', '2022-04-08 09:46:00', '2022-04-22 09:46:00', 0, '2022-04-07 19:48:29', '2022-04-12 08:06:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dials`
--

CREATE TABLE `dials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `turn` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `dials`
--

INSERT INTO `dials` (`id`, `user_id`, `campaign_id`, `turn`, `deleted`, `created_at`, `updated_at`) VALUES
(1, '1', 3, 0, 1, '2022-04-07 03:25:36', '2022-04-12 07:06:44'),
(2, '1', 4, 60, 0, '2022-04-07 19:48:48', '2022-04-08 10:10:03'),
(3, '3', 4, 0, 0, '2022-04-09 10:38:52', '2022-04-12 08:07:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `list_products`
--

CREATE TABLE `list_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `luckywheel_histories`
--

CREATE TABLE `luckywheel_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `luckywheel_histories`
--

INSERT INTO `luckywheel_histories` (`id`, `campaign_id`, `user_id`, `product_id`, `name`, `phone`, `product`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 13, '', '', '13', 0, '2022-04-07 19:32:53', '2022-04-07 19:32:53'),
(2, 0, NULL, 16, '', '', '16', 0, '2022-04-07 19:33:05', '2022-04-07 19:33:05'),
(3, 0, NULL, 11, 'user1', '365254885', '11', 0, '2022-04-07 19:34:51', '2022-04-07 19:34:51'),
(4, 0, NULL, 18, 'user1', '365254885', 'sp8', 0, '2022-04-07 19:41:48', '2022-04-07 19:41:48'),
(5, 4, 1, 23, 'user1', '365254885', 't5', 0, '2022-04-08 03:10:16', '2022-04-08 03:10:16'),
(6, 4, 1, 21, 'user1', '365254885', 't3', 0, '2022-04-08 03:10:24', '2022-04-08 03:10:24'),
(7, 4, 1, 20, 'user1', '365254885', 't2', 0, '2022-04-08 03:10:32', '2022-04-08 03:10:32'),
(8, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 03:10:40', '2022-04-08 03:10:40'),
(9, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 03:10:48', '2022-04-08 03:10:48'),
(10, 4, 1, 19, 'user1', '365254885', 't1', 0, '2022-04-08 03:10:56', '2022-04-08 03:10:56'),
(11, 4, 1, 25, 'user1', '365254885', 't7', 0, '2022-04-08 03:11:04', '2022-04-08 03:11:04'),
(12, 4, 1, 23, 'user1', '365254885', 't5', 0, '2022-04-08 03:11:41', '2022-04-08 03:11:41'),
(13, 4, 1, 21, 'user1', '365254885', 't3', 0, '2022-04-08 03:20:08', '2022-04-08 03:20:08'),
(14, 4, 1, 25, 'user1', '365254885', 't7', 0, '2022-04-08 03:20:43', '2022-04-08 03:20:43'),
(15, 4, 1, 23, 'user1', '365254885', 't5', 0, '2022-04-08 03:20:51', '2022-04-08 03:20:51'),
(16, 4, 1, 19, 'user1', '365254885', 't1', 0, '2022-04-08 03:20:59', '2022-04-08 03:20:59'),
(17, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 03:21:07', '2022-04-08 03:21:07'),
(18, 4, 1, 26, 'user1', '365254885', 't8', 0, '2022-04-08 03:21:15', '2022-04-08 03:21:15'),
(19, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 03:21:22', '2022-04-08 03:21:22'),
(20, 4, 1, 26, 'user1', '365254885', 't8', 0, '2022-04-08 03:59:32', '2022-04-08 03:59:32'),
(21, 4, 1, 25, 'user1', '365254885', 't7', 0, '2022-04-08 03:59:40', '2022-04-08 03:59:40'),
(22, 4, 1, 26, 'user1', '365254885', 't8', 0, '2022-04-08 04:26:55', '2022-04-08 04:26:55'),
(23, 4, 1, 26, 'user1', '365254885', 't8', 0, '2022-04-08 04:27:24', '2022-04-08 04:27:24'),
(24, 4, 1, 20, 'user1', '365254885', 't2', 0, '2022-04-08 04:46:58', '2022-04-08 04:46:58'),
(25, 4, 1, 23, 'user1', '365254885', 't5', 0, '2022-04-08 04:47:07', '2022-04-08 04:47:07'),
(26, 4, 1, 26, 'user1', '365254885', 't8', 0, '2022-04-08 04:57:15', '2022-04-08 04:57:15'),
(27, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 04:58:20', '2022-04-08 04:58:20'),
(28, 4, 1, 25, 'user1', '365254885', 't7', 0, '2022-04-08 04:59:46', '2022-04-08 04:59:46'),
(29, 4, 1, 25, 'user1', '365254885', 't7', 0, '2022-04-08 04:59:57', '2022-04-08 04:59:57'),
(30, 4, 1, 25, 'user1', '365254885', 't7', 0, '2022-04-08 05:00:06', '2022-04-08 05:00:06'),
(31, 4, 1, 23, 'user1', '365254885', 't5', 0, '2022-04-08 05:01:16', '2022-04-08 05:01:16'),
(32, 4, 1, 23, 'user1', '365254885', 't5', 0, '2022-04-08 05:01:25', '2022-04-08 05:01:25'),
(33, 4, 1, 23, 'user1', '365254885', 't5', 0, '2022-04-08 05:01:59', '2022-04-08 05:01:59'),
(34, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:02:22', '2022-04-08 05:02:22'),
(35, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:02:30', '2022-04-08 05:02:30'),
(36, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:02:39', '2022-04-08 05:02:39'),
(37, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:02:48', '2022-04-08 05:02:48'),
(38, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:07:24', '2022-04-08 05:07:24'),
(39, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:07:36', '2022-04-08 05:07:36'),
(40, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:07:45', '2022-04-08 05:07:45'),
(41, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:09:43', '2022-04-08 05:09:43'),
(42, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:10:13', '2022-04-08 05:10:13'),
(43, 4, 1, 22, 'user1', '365254885', 't4', 0, '2022-04-08 05:10:34', '2022-04-08 05:10:34'),
(44, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 05:11:06', '2022-04-08 05:11:06'),
(45, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 05:11:27', '2022-04-08 05:11:27'),
(46, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 05:12:24', '2022-04-08 05:12:24'),
(47, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 05:42:01', '2022-04-08 05:42:01'),
(48, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 08:31:24', '2022-04-08 08:31:24'),
(49, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 09:23:45', '2022-04-08 09:23:45'),
(50, 4, 1, 24, 'user1', '365254885', 't6', 0, '2022-04-08 10:10:03', '2022-04-08 10:10:03'),
(51, 3, 1, 15, 'user1', '365254885', 'sp5', 0, '2022-04-09 02:25:12', '2022-04-09 02:25:12'),
(52, 3, 1, 11, 'user1', '365254885', 'sp1', 0, '2022-04-09 02:25:24', '2022-04-09 02:25:24'),
(53, 3, 1, 14, 'user1', '365254885', 'sp4', 0, '2022-04-09 02:25:56', '2022-04-09 02:25:56'),
(54, 4, 3, 24, 'Nguyễn Minh Nhật', '', 't6', 0, '2022-04-12 08:06:41', '2022-04-12 08:06:41'),
(55, 4, 3, 24, 'Nguyễn Minh Nhật', '', 't6', 0, '2022-04-12 08:06:54', '2022-04-12 08:06:54'),
(56, 4, 3, 24, 'Nguyễn Minh Nhật', '', 't6', 0, '2022-04-12 08:07:03', '2022-04-12 08:07:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(114, '2014_10_12_000000_create_users_table', 1),
(115, '2014_10_12_100000_create_password_resets_table', 1),
(116, '2019_08_19_000000_create_failed_jobs_table', 1),
(117, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(118, '2022_04_01_064747_create_list_products_table', 1),
(119, '2022_04_01_065102_create_products_table', 1),
(120, '2022_04_01_065914_create_luckywheel_histories_table', 1),
(121, '2022_04_04_030004_create_dials_table', 1),
(122, '2022_04_04_030251_create_campaigns_table', 1),
(123, '2022_04_09_151432_create_admins_table', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `quantity`, `unit`, `image`, `campaign_id`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'test1', 1, NULL, 'pic08.jpg', 1, 0, '2022-04-07 01:35:19', '2022-04-07 01:35:19'),
(2, 'test2', 1, NULL, '164932064465.jpg', 1, 0, '2022-04-07 01:37:24', '2022-04-07 01:37:24'),
(3, 'pt1', 3, NULL, '16493216634.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(4, 'pt2', 4, NULL, '164932166310.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(5, 'pt3', 9, NULL, '164932166349.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(6, 'pt4', 2, NULL, '164932166324.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(7, 'pt5', 9, NULL, '164932166393.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(8, 'pt6', 1, NULL, '164932166316.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(9, 'pt', 8, NULL, '164932166382.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(10, 'pt8', 25, NULL, '164932166313.jpg', 2, 0, '2022-04-07 01:54:23', '2022-04-07 01:54:23'),
(11, 'sp1', 12, NULL, '164932461693.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-12 08:37:44'),
(12, 'sp2', 77, NULL, '164932461619.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-07 02:43:36'),
(13, 'sp3', 3, NULL, '164932461689.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-07 02:43:36'),
(14, 'sp4', 32, NULL, '164932461619.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-09 02:25:56'),
(15, 'sp5', 4, NULL, '164932461624.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-09 02:25:12'),
(16, 'sp6', 6, NULL, '164932461686.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-07 02:43:36'),
(17, 'sp7', 8, NULL, '164932461659.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-07 02:43:36'),
(18, 'sp8', 11, NULL, '164932461674.jpg', 3, 0, '2022-04-07 02:43:36', '2022-04-07 02:43:36'),
(19, 't1', 0, NULL, '164938610977.jpg', 4, 0, '2022-04-07 19:48:29', '2022-04-08 03:20:59'),
(20, 't2', 0, NULL, '164938610910.jpg', 4, 0, '2022-04-07 19:48:29', '2022-04-08 04:46:58'),
(21, 't3', 0, NULL, '16493861094.jpg', 4, 0, '2022-04-07 19:48:29', '2022-04-08 03:20:08'),
(22, 't4', 0, NULL, '164938610965.jpg', 4, 0, '2022-04-07 19:48:29', '2022-04-08 05:10:34'),
(23, 't5', 0, NULL, '164938610951.jpg', 4, 0, '2022-04-07 19:48:29', '2022-04-08 05:01:59'),
(24, 't6', 0, NULL, '164938610999.jpg', 4, 0, '2022-04-07 19:48:29', '2022-04-12 08:07:03'),
(25, 't7', 0, NULL, '164938610969.png', 4, 0, '2022-04-07 19:48:29', '2022-04-08 05:00:06'),
(26, 't8', 0, NULL, '16493861097.jpg', 4, 0, '2022-04-07 19:48:29', '2022-04-08 04:57:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_server` text COLLATE utf8mb4_unicode_ci,
  `userName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sponsor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sponsorId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sponsorAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sponsorFloor` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `placement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placementId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placementAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placementFloor` int(11) DEFAULT NULL,
  `type` enum('f0','f1','f2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'f0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`password`, `remember_token`, `id`, `id_server`, `userName`, `fullName`, `phoneNumber`, `email`, `avatar`, `sponsor`, `sponsorId`, `sponsorAddress`, `sponsorFloor`, `active`, `placement`, `placementId`, `placementAddress`, `placementFloor`, `type`, `deleted`, `created_at`, `updated_at`) VALUES
('$2y$10$WqTNtjkdNOR71zccDXwWpuBsn1rh9yekU4Mo3bGBMtVR0v60wM9Oi', NULL, 1, NULL, 'user1', 'user1', '365254885', NULL, NULL, 'user1', 'user1', 'user1', 1, 0, 'user1', 'user1', 'user1', 1, 'f0', 0, '2022-04-07 00:46:14', '2022-04-07 00:46:14'),
(NULL, NULL, 2, NULL, 'nmhasd123', 'Nguyễn Minh Nhật', NULL, 'nmhasd123@gmail.com', NULL, NULL, '3d2f92f2-21c0-405d-b6d8-4f4999c7cf2f', '0-2-6-35-18-1-14', 6, 0, NULL, NULL, NULL, NULL, 'f0', 0, '2022-04-09 04:01:05', '2022-04-09 04:01:05'),
(NULL, NULL, 3, '2881061d-b233-4077-b500-37747971d6ed', 'nmhasd123', 'Nguyễn Minh Nhật', NULL, 'nmhasd123@gmail.com', NULL, NULL, '3d2f92f2-21c0-405d-b6d8-4f4999c7cf2f', '0-2-6-35-18-1-14', 6, 0, NULL, NULL, NULL, NULL, 'f0', 0, '2022-04-09 04:58:06', '2022-04-09 04:58:06');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Chỉ mục cho bảng `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `dials`
--
ALTER TABLE `dials`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `list_products`
--
ALTER TABLE `list_products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `luckywheel_histories`
--
ALTER TABLE `luckywheel_histories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `dials`
--
ALTER TABLE `dials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `list_products`
--
ALTER TABLE `list_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `luckywheel_histories`
--
ALTER TABLE `luckywheel_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
