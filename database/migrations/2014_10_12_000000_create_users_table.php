<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('password')->nullable();
            $table->rememberToken();
            //add
            $table->id();
            $table->string('id_server');
            $table->string('userName');
            $table->string('fullName');
            $table->string('phoneNumber')->nullable();
            $table->string('email')->nullable();
            $table->string('avatar')->nullable();
            $table->string('sponsor')->nullable();
            $table->string('sponsorId')->nullable();
            $table->string('sponsorAddress')->nullable();
            $table->integer('sponsorFloor')->nullable();
            $table->boolean('active')->default(0);
            $table->string('placement')->nullable();
            $table->string('placementId')->nullable();
            $table->string('placementAddress')->nullable();
            $table->integer('placementFloor')->nullable();
            $table->enum('type',['f0', 'f1', 'f2'])->default('f0');
            $table->boolean('deteled')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
